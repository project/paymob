<?php

namespace Drupal\commerce_paymob\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Url;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Symfony\Component\HttpFoundation\Request;
use Paymob\Library\Paymob;

class CallbackController extends ControllerBase {

    public function index(Request $request) {
        if (Paymob::filterVar('REQUEST_METHOD', 'SERVER') === 'POST') {
            $pluginlog = \Drupal::root() . '/' . \Drupal::moduleHandler()->getModule('commerce_paymob')->getPath() . '/paymob.log';
            $post_data = file_get_contents('php://input');
            $json_data = json_decode($post_data, true);
            $orderId = Paymob::getIntentionId($json_data['intention']['extras']['creation_extras']['merchant_intention_id']);
            $order = \Drupal\commerce_order\Entity\Order::load($orderId);
            $payment_gateway = $order->get('payment_gateway')->entity;
            /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
            if ($payment_gateway) {
                $payment_gateway_plugin = $payment_gateway->getPlugin();
            } else {
                // Handle the case where the payment gateway entity is not found.
                die("Payment gateway entity not found for order: $orderId");
            }
            if (!$payment_gateway_plugin instanceof OffsitePaymentGatewayInterface) {
                die("Ops, you are accessing wrong data");
            }
            $configuration = $payment_gateway_plugin->getConfiguration();
            $secretkey = $configuration['secret_key'];
            $hmac = $configuration['hmac'];
            $debug = $configuration['debug'];

            $database = \Drupal::database();
            $fields = [
                'paymob_intention_id',
                'paymob_cents_amount',
            ];
            $query = $database->select('paymob_order_info', 't')
                    ->fields('t', $fields)
                    ->condition('drupal_order_id', $orderId)
                    ->orderBy('id', 'DESC')
                    ->range(0, 1); // Optional: Limit the result to one row.
            $result = $query->execute()->fetchAssoc();
            $OrderIntensionId = !empty($result['paymob_intention_id']) ? $result['paymob_intention_id'] : null;
            $OrderAmount = !empty($result['paymob_cents_amount']) ? $result['paymob_cents_amount'] : null;
            if ($OrderIntensionId != $json_data['intention']['id']) {
                die("intension ID is not matched for order : $orderId");
            }

            if ($OrderAmount != $json_data['intention']['intention_detail']['amount']) {
                die("intension amount are not matched for order : $orderId");
            }
            $country = Paymob::getCountryCode($secretkey);

            $cents = 100;
            if ($country == 'omn') {
                $cents = 1000;
            }
            if (!Paymob::verifyHmac($hmac, $json_data, array('id' => $OrderIntensionId, 'amount' => $OrderAmount, 'cents' => $cents))) {
                die("can not verify order: $orderId");
            }
            // Check if the order exists.
            if ($order instanceof OrderInterface) {
                $msg = 'Order #' . $orderId;
                if (!empty($json_data['transaction'])) {
                    $trans = $json_data['transaction'];
                    if (
                            $trans['success'] === true &&
                            $trans['is_voided'] === false &&
                            $trans['is_refunded'] === false &&
                            $trans['is_capture'] === false
                    ) {
                        $note = 'Paymob  Webhook: Payment Approved';
                        $msg = $msg . ' ' . $note;
                        $status = 'completed'; // Replace with the desired status.
                        $order->set('state', $status);
                        $order->unlock();
                        $order->save();
                        $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
                        $payment = $payment_storage->create([
                            'state' => $status,
                            'amount' => $order->getTotalPrice(),
                            'payment_gateway' => 'paymob',
                            'order_id' => $order->id(),
                            'remote_id' => $json_data['transaction']['id'],
                            'remote_state' => $status,
                        ]);
                        // Save the payment entity.
                        $payment->save();
                        Paymob::addLogs($debug, $pluginlog, $msg);
                    } else {
                        $note = 'Paymob  Webhook: Payment is not completed';
                        $msg = $msg . ' ' . $note;
                        $status = 'canceled'; // Replace with the desired status.
                        $order->set('state', $status);
                        $order->save();
                        Paymob::addLogs($debug, $pluginlog, $msg);
                    }
                    die("Order updated: $orderId");
                }
            } else {
                die("Ops, you are accessing wrong data");
            }
        } else if (Paymob::filterVar('REQUEST_METHOD', 'SERVER') === 'GET') {
            $orderId = Paymob::getIntentionId(Paymob::filterVar('merchant_order_id'));
            if (
                    Paymob::filterVar('success') === "true" &&
                    Paymob::filterVar('is_voided') === "false" &&
                    Paymob::filterVar('is_refunded') === "false"
            ) {
                $url = Url::fromRoute('commerce_payment.checkout.return', ['commerce_order' => $orderId]);
            } else {
                $url = Url::fromRoute('commerce_payment.checkout.cancel', ['commerce_order' => $orderId]);
            }
            $url->setRouteParameter('step', 'payment');
            $url->setAbsolute();
            $returnurl = $url->toString();
            $returnurl .= '?' . http_build_query($_GET);
            header("Location: " . $returnurl);
            die;
        }
    }
}
