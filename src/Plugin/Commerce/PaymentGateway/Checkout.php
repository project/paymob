<?php

namespace Drupal\commerce_paymob\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Core\Url;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Paymob\Library\Paymob;
use Drupal\commerce_payment\Exception\PaymentGatewayException;

/**
 * Provides the Paymob offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "commerce_paymob",
 *   label = @Translation("Paymob"),
 *   display_label = @Translation("Paymob"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_paymob\PluginForm\RedirectCheckoutForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa", 
 *   },
 * )
 */
class Checkout extends OffsitePaymentGatewayBase
{
    protected $plugin_url;
    protected $log_path;
    public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
        $this->plugin_url = \Drupal::root() . '/' . \Drupal::moduleHandler()->getModule('commerce_paymob')->getPath() . '/';
        $this->log_path = $this->plugin_url . 'paymob.log';
    }
    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration()
    {
        return [
            'secret_key' => '',
            'public_key' => '',
            'api_key' => '',
            'integration_ids' => '',
            'hmac' => '',
            'callback_url' => '',
            'debug' => '0'
        ] +
            parent::defaultConfiguration();
    }
    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form1, FormStateInterface $form_state)
    {
        $form = parent::buildConfigurationForm($form1, $form_state);
        $callBackUrlObj = Url::fromUri('route:commerce_paymob.callback');
        $callBackUrlObj->setAbsolute();
        $callBackUrl = $callBackUrlObj->toString();
        
        $form['secret_key'] = [
            '#type' => 'textfield',
            '#title' => $this->t("Secret Key"),
            '#default_value' => $this->configuration['secret_key'],
            '#required' => true,
        ];
        $form['public_key'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Public Key'),
            '#default_value' => $this->configuration['public_key'],
            '#required' => true,
        ];
        $form['api_key'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Api Key'),
            '#default_value' => $this->configuration['api_key'],
            '#required' => true,
        ];
        $form['integration_ids'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Payment method(s)'),
            '#description' => $this->t('Add the Payment methods ID(s) that exist in Paymob account separated by comma , separator. (Example: 123456,98765,45678)'),
            '#default_value' => $this->configuration['integration_ids'],
            '#required' => true,
        ];
        $form['hmac'] = [
            '#type' => 'textfield',
            '#title' => $this->t('HMAC'),
            '#default_value' => $this->configuration['hmac'],
            '#required' => true,
        ];

        $form['callback_url'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Callback URL'),
            '#description' => $this->t('Please add the above URL in Paymob Merchant account for both callback and response URLs Settings for each payment method.'),
            '#default_value' => $callBackUrl,
            '#attributes' => ['readonly' => 'readonly'],
        ];
        $form['debug'] = [
            '#type' => 'select',
            '#title' => $this->t('Debug Log'),
            '#description' => $this->t('Log file will be saved in ' . $this->plugin_url),
            '#options' => [
                '1' => $this->t('Yes'),
                '0' => $this->t('No'),
            ],
            '#default_value' => $this->configuration['debug'],
        ];
        $form['mode'] = [
            '#type' => 'value',
            '#value' => 'n/a',
        ];
        $form['mode']['#access'] = FALSE;
        return $form;
    }
    /**
     * {@inheritdoc}
     */
    public function validateConfigurationForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValue($form['#parents']);
        $pubKey = $values['public_key'];
        $secKey = $values['secret_key'];
        $apiKey = $values['api_key'];
        $hmac = $values['hmac'];
        $debug = $values['debug'];
        $integration_id = $values['integration_ids'];
        if (empty($pubKey) || empty($secKey) || empty($apiKey) || empty($hmac) || empty($integration_id)) {
            $error = 'Paymob Error : Please, ensure filling the secret, public, API keys, HMAC, and integration ID(s) that exist in Paymob merchant account.';
            return $form_state->setError($form['api_key'], $this->t($error));
        }
        $conf['apiKey'] = $apiKey;
        $conf['pubKey'] = $pubKey;
        $conf['secKey'] = $secKey;
        try {
            $paymobReq = new Paymob($debug, $this->log_path);
            $result = $paymobReq->authToken($conf);
            $note = 'Merchant configuration: ';
            Paymob::addLogs($debug, $this->log_path, $note, $result);
            if ($hmac !== $result['hmac']) {
                $error = 'Paymob Error : Please provide correct HMAC key that exist in Paymob merchant account.';
                return $form_state->setError($form['hmac'], $this->t($error));
            }
            $integrations = explode(',', $integration_id);
            $confIntegration = [];

            foreach ($integrations as $id) {
                $id = (int) trim($id);
                if ($id > 0) {
                    array_push($confIntegration, $id);
                }
            }
            $availIds = [];
            foreach ($result['integrationIDs'] as $integration) {
                array_push($availIds, $integration['id']);
            }

            foreach ($confIntegration as $val) {
                if (array_search($val, $availIds) === false) {
                    $error = 'Paymob Error : one or all of the payment methods provided do not match Paymob flash/unified checkout integration. Please re-check or you can contact your Paymob account manager to switch the payment methods into flash integration..';
                    return $form_state->setError($form['integration_ids'], $this->t($error));
                }
            }

        } catch (\Exception $exc) {
            $error = 'Paymob Error :' . $exc->getMessage();
            return $form_state->setError($form['api_key'], $this->t($error));
        }
        return false;
    }
    /**
     * {@inheritdoc}
     */
    public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitConfigurationForm($form, $form_state);
        if (!$form_state->getErrors()) {
            $values = $form_state->getValue($form['#parents']);
            $this->configuration['secret_key'] = $values['secret_key'];
            $this->configuration['public_key'] = $values['public_key'];
            $this->configuration['api_key'] = $values['api_key'];
            $this->configuration['integration_ids'] = $values['integration_ids'];
            $this->configuration['hmac'] = $values['hmac'];
            $this->configuration['debug'] = $values['debug'];
        }
    }
    /**
     * 
     * @param OrderInterface $order
     * @param Request $request
     */
    public function onCancel(OrderInterface $order, Request $request)
    {
        $orderId = Paymob::getIntentionId(Paymob::filterVar('merchant_order_id'));
        if (!Paymob::verifyHmac($this->configuration['hmac'], $_GET)) {
            throw new PaymentGatewayException('Ops, you are accessing wrong data');
        }
        $note = 'Paymob: Payment is not completed';
        $msg = 'In callback action, for order #' . $orderId . ' ' . $note;
        Paymob::addLogs($this->configuration['debug'], $this->log_path, $msg);
        $this->messenger()->addError($this->t($note, ['@gateway' => $this->getDisplayLabel()]));
    }
    /**
     * 
     * @param OrderInterface $order
     * @param Request $request
     */
    public function onReturn(OrderInterface $order, Request $request)
    {
        $orderId = Paymob::getIntentionId(Paymob::filterVar('merchant_order_id'));
        if (!Paymob::verifyHmac($this->configuration['hmac'], $_GET)) {
            throw new PaymentGatewayException('Ops, you are accessing wrong data');
        }
        $t_id = Paymob::filterVar('id');
        $t_msg = Paymob::filterVar('data_message');
        $this->createPayment($order, $t_id, $t_msg, 'completed');
        Paymob::addLogs($this->configuration['debug'], $this->log_path, ' In Callback action, for order# ' . $orderId, json_encode($_GET));
        $note = 'Paymob : Payment Approved';
        $msg = 'In callback action, for order #' . $orderId . ' ' . $note;
        Paymob::addLogs($this->configuration['debug'], $this->log_path, $msg);
        $this->messenger()->addStatus($note);
    }
    function createPayment(OrderInterface $order, $t_id, $tstatus, $state)
    {

        /** @var \Drupal\commerce_payment\PaymentStorageInterface $paymentStorage */
        $paymentStorage = $this->entityTypeManager->getStorage('commerce_payment');
        /** @var PaymentInterface $payment */
        $payment = $paymentStorage->create([
            'state' => $state,
            'amount' => $order->getTotalPrice(),
            'payment_gateway' => $this->entityId,
            'order_id' => $order->id(),
            'remote_id' => $t_id,
            'remote_state' => $tstatus,
        ]);
        $payment->save();
    }
}
