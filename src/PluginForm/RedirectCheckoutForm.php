<?php

namespace Drupal\commerce_paymob\PluginForm;

use Paymob\Library\Paymob;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Database\Database;
class RedirectCheckoutForm extends BasePaymentOffsiteForm
{
  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form1, FormStateInterface $form_state)
  {

    $form = parent::buildConfigurationForm($form1, $form_state);
    $pluginlog = \Drupal::root() . '/' . \Drupal::moduleHandler()->getModule('commerce_paymob')->getPath() . '/paymob.log';
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();
    $orderId = $order->id();
    $profile = $order->getBillingProfile();
    $billing_address = $profile->get('address')->first();
    // Payment gateway configuration data.
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();
    $secretkey = $configuration['secret_key'];
    $publickey = $configuration['public_key'];
    $hmac = $configuration['hmac'];
    $debug = $configuration['debug'];
    $integrationIds = $configuration['integration_ids'];
    $integrations = explode(',', $integrationIds);
    $integration_ids = [];
    foreach ($integrations as $id) {
      $id = (int) $id;
      if ($id > 0) {
        array_push($integration_ids, $id);
      }
    }
    $country_list = \Drupal::service('address.country_repository')->getList();
    $country = $country_list[$billing_address->getCountryCode()];
    if ($profile->hasField('telephone')) {
      $telephone = $profile->get('telephone')->value;
    } else if ($profile->hasField('field_phone')) {
      $telephone = $profile->get('field_phone')->value;
    } else {
      $telephone = null;
    }
    $billing = [
      "email" => $order->getEmail(),
      "first_name" => $billing_address->getGivenName(),
      "last_name" => $billing_address->getFamilyName(),
      "street" => !empty($billing_address->getAddressLine1()) ? $billing_address->getAddressLine1() : 'NA',
      "phone_number" => !empty($telephone) ? $telephone : 'NA',
      "city" => !empty($billing_address->getLocality()) ? $billing_address->getLocality() : 'NA',
      "country" => !empty($country) ? $country : 'NA',
      "state" => !empty($country) ? $country : 'NA',
      "postal_code" => !empty($billing_address->getPostalCode()) ? $billing_address->getPostalCode() : 'NA',
    ];

    $currency = $payment->getAmount()->getCurrencyCode();
    $price = $payment->getAmount()->getNumber();
    $country = Paymob::getCountryCode($secretkey);
    $cents = 100;
    $round = 2;
    if ($country == 'omn') {
      $cents = 1000;
    }

    $price = round((round($price, $round)) * $cents, $round);
    $data = [
      "amount" => $price,
      "currency" => $currency,
      "payment_methods" => $integration_ids,
      "billing_data" => $billing,
      "extras" => ["merchant_intention_id" => $orderId . '_' . time()],
      "special_reference" => $orderId . '_' . time()
    ];

    $paymobReq = new Paymob($debug, $pluginlog);
    $status = $paymobReq->createIntention($secretkey, $data, $orderId);
    if (!$status['success']) {
      \Drupal::messenger()->addError($status['message']);
      return;
    } else {
      $countryCode = $paymobReq->getCountryCode($secretkey);
      $apiUrl = $paymobReq->getApiUrl($countryCode);
      $cs = $status['cs'];
      $to = Url::fromUri($apiUrl . "unifiedcheckout/?publicKey=$publickey&clientSecret=$cs")->toString();
      $data = [
        'drupal_order_id' => $orderId,
        'paymob_intention_id' => $status['intentionId'],
        'paymob_cents_amount' => $status['centsAmount'],
      ];
      $connection = Database::getConnection();
      $connection->insert('paymob_order_info')
        ->fields($data)
        ->execute();
      return $this->buildRedirectForm(
        $form,
        $form_state,
        $to,
        [],
        self::REDIRECT_GET
      );
    }
  }
}
