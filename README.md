# Paymob Payment Gateway package for Drupal Commerce

## Installation
1. Install the Paymob Payment module for Drupal Commerce e-commerce via [paymob_drupal/commerce_paymob](https://packagist.org/packages/paymob_drupal/commerce_paymob) composer.
```bash
composer require paymob_drupal/commerce_paymob
```

2. In the admin panel, Extend tab, search for the Paymob module, select it, and click the install button to install it.

## Configuration
### Paymob Account
1. Login to the Paymob account → Setting in the left menu. 
2. Get the Secret, public, API keys, HMAC and Payment Methods IDs (integration IDs).

### Drupal commerce Admin Configuration
1. In Drupal commerce Admin Panel Menu Commerce→ Configuration→ Payments→ Payment Gateways section. 
2. Click on Add Payment gateway and Select Paymob payment, paste each key in its place in the setting page. 
3. Please ensure adding the integration IDs separated by comma ,. These IDs will be shown in the Paymob payment page. 
4. Copy integration callback URL that exists in Paymob Drupal commerce setting page. Then, paste it into each payment integration/method in Paymob account.

## Checkout page 
Paymob payment method will be shown for the end-user to select and start his payment process. 
